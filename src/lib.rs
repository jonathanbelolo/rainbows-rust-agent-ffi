use lazy_static::lazy_static;
use mut_static::MutStatic;

use core::did::peer::Identity;
use core::agent::store::Store;

use std::ffi::{CString};
use std::os::raw::{c_char};

lazy_static! {
    pub static ref STORE: MutStatic<Store> = {
        MutStatic::new()
    };
}

#[no_mangle]
pub extern "C" fn init_agent() {
    core::init();
    let mut root = Identity::new("Super Root".to_owned());
    root.root_doc.add_endpoint("localhost:3030".into());
    STORE.set(Store::new(root)).unwrap();
}

#[no_mangle]
pub extern "C" fn ping_agent() {
    println!("Hello from Store: {}", STORE.read().unwrap().root_identity.nick);
}

#[no_mangle]
pub extern "C" fn create_invitation() -> *mut c_char {
    let (invite, _handle) = STORE.write().unwrap().create_invitation();
    let cstring = CString::new(invite.clone()).unwrap();
    cstring.into_raw()
}


#[no_mangle]
pub extern "C" fn accept_invitation() -> *mut c_char {
    let (invite, _handle) = STORE.write().unwrap().create_invitation();
    let cstring = CString::new(invite.clone()).unwrap();
    cstring.into_raw()
}

#[no_mangle]
pub extern "C" fn free_string(s: *mut c_char) {
    let cstring = unsafe { CString::from_raw(s) };
    drop(cstring); // not technically required but shows what we're doing
}

#[repr(C)]
pub enum Event {
    Ping
}